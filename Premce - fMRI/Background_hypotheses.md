### Background and rationale of the Study

We constantly make predictions by extracting regularities in the environment from past experiences. 
However,in some conditions the past does not always predict the future. 
For example, when the environment changes predictions based on remote events are no longer informative, while recent outcomes are more important. Therefore, predictions need to be based on recent experiences.
There is evidence  that participants learn faster after a change in the environment, as current outcomes have greater impact on future choices (Behrens et al., 2007; McGuire, Nassar, Gold, & Kable, 2014).
After a change in the environment has been detected, people should favour recent events to make predictions. What it is still not clear is whether this increased update of the internal models used to make predictions translate in a similar increase in encoding of the details of episodes that follow a change in the environment.  
While in condition of stability predictions should come from brain areas that process the extraction of similarities among experiences from an extended period, in conditions of high unexpected uncertainty which follow change points (Soltani & Izquierdo, 2019) rapid learning rate should be made possible by the activity of the hippocampus (or of precise hippocampal subfields). In fact, the hippocampus, and in particular the DG/CA3 subregion, is responsible for rapid learning of episodes and for binding together the elements of a stimulus. 

### Hypothesis / Analyses suggested
## Main hypotheses
1. Activation of the hippocampus should be enhanced, compared to the activation of semantic areas, for remembered vs forgotten items, after a change point compared to before a change point.       
![HC-semantic](Images/HC%20-%20semantic.png)      
We are not sure about the pattern before the change point. An alternative hypothesis would be the following:        
![HC-semantic2](Images/HC-semantic%20alternative.png)      

Disantantages: Hippocampal activations has not been consistently shown in the literature as supporting increased item memory. In fact, hippocampal activation is related more to the ability to bind elements together. In constrast, perirhinal cortex has been related to the memory of the "what", and might support encoding of the items.  
As a consequence, it is possible that the effect hypothesised is in the hippocampal subfields: DG/CA3 vs CA1.       
![subfields](Images/Subfields.png)      

2. Is the activity of the HC related to the neural substrates of prediciton?
In order to answer this question, we will compare the multivariate activation of the category template to the trials of the encoding phase to quantify the amount of prediction for each trial by participant and relate it to HC activation (or DG/CA3 activation) and then to memory. 
![subfields](Images/RSA%20hypotheses.png)      


## Secondary hypotheses
3. Is the effect due to signed PE, unsigned PE, or learning rate? 
Analysis: Add the tree predictors as regressors in a GLM to predict activation of HC (DG/CA3). 
4. Do different areas in the brain process different learning rates? 
Analysis: Add continuous LR as predictor in a GLM. Alternatively, split the continuous LR into fast and slow learning rate and check the activation of different ROIs (HC, semantic areas, uncertainty-related areas), as in Meder et al., (2017). 
5. Is there an increased co-activation between the HC (or DG/CA3) and the uncertainty-related ares (Locus Coeruleus, ACC) during periods of high uncertainty? 
Analysis: PPI or beta series correlation between the two areas, for before and after a change point



### References
Behrens, T. E. J., Woolrich, M. W., Walton, M. E., & Rushworth, M. F. S. (2007). Learning the value of information in an uncertain world. Nature Neuroscience, 10(9), 1214–1221. https://doi.org/10.1038/nn1954     
McGuire, J. T., Nassar, M. R., Gold, J. I., & Kable, J. W. (2014). Functionally Dissociable Influences on Learning Rate in a Dynamic Environment. Neuron, 84(4), 870–881. https://doi.org/10.1016/j.neuron.2014.10.013      
Meder, D., Kolling, N., Verhagen, L., Wittmann, M. K., Scholl, J., Madsen, K. H., Hulme, O. J., Behrens, T. E. J., & Rushworth, M. F. S. (2017). Simultaneous representation of a spectrum of dynamically changing value estimates during decision making. Nature Communications, 8(1). https://doi.org/10.1038/s41467-017-02169-w      
Soltani, A., & Izquierdo, A. (2019). Adaptive learning under expected and unexpected uncertainty. In Nature Reviews Neuroscience (Vol. 20, Issue 10, pp. 635–644). Nature Publishing Group. https://doi.org/10.1038/s41583-019-0180-y       
