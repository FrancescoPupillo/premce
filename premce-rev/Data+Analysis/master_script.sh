# First, rename the files so that the first participants are "01" rather than "0" and copy to the "data_files" folder
Rscript helper_functions/renameFiles.R
Rscript analysis_files/01.analysis_day1.R
Rscript analysis_files/02.analysis_day2-contingencies.R
Rscript analysis_files/03.item_analysis.R

