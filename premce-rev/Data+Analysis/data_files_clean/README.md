


## Code book - Description of the variables 

## *ses-01_part-01 and ses-01_part-01*

### participant
participant number

### pract_resp.keys
key pressed by participant as a response in the practice trials

### pract_resp.corr
binary variable indicating whether the response was correct (1) or incorrect (0). In the practice trials. 

### first_character
The first character that appeared on each trial

### second_character
The second character that appeared on each trial

### Cue
Number appearing after the presentation of the two characters on every trial telling participants for which character we want them to make the prediction. 

### corr_ans
The correct response for each trial. 

### obj_category
Category object displayed at the end of the trial. 

### image
Image displayed at the end of the trial.

### trial_cond
Binary variable indicating whether it is a trial that occurs 85% of the time (1) or 15% of the time (0)

### task_resp.keys
Keys pressed by participants in the actual trials

### task_resp.corr
Variable indicating whether a reponse in the task was correct (1) or incorrect (0)

### task_resp.rt
Reaction time for every response

### switch_cond
Numeric variable indicating the block. The levels are the following:
99: warm-up block in ses-02_part-01
2: list A-plus (list A repeated on Day2)
3: list B. List that occurs after the first switch
4: List C . List that occurs after the second switch
5: List D List that occurs after the third switch



## *ses-01_part-01 and ses-02_part-01*

### participant
participant number

### warmup_resp.keys
key pressed by participant as a response in the warmup trials

### warmup_resp.corr
binary variable indicating whether the response was correct (1) or incorrect (0). In the warmpu trials. 

### warmup_resp.rt
Rt for the warmup trials

### first_character
The first character that appeared on each trial

### second_character
The second character that appeared on each trial

### Cue
Number appearing after the presentation of the two characters on every trial telling participants for which character we want them to make the prediction. 

### left_categ
For each trial, the category that appeared on the first position on the left

### centleft_categ
For each trial, the category that appered on the second position on the left

### centright_categ
For each trial, the categor that appeared on the third position starting from the left side of the screen. 

### right categ
For each trial, the category that apperead on the fourth position starting from the left side of the screen. 

### corr_ans
The correct response for each trial. 

### obj_category
Category object displayed at the end of the trial. 

### image
Image displayed at the end of the trial.

### trial_cond
Binary variable indicating whether it is a trial that occurs 85% of the time (1) or 15% of the time (0)

### task_resp.keys
Keys pressed by participants in the actual trials

### task_resp.corr
Variable indicating whether a reponse in the task was correct (1) or incorrect (0)

### task_resp.rt
Reaction time for every response

### switch_cond
Numeric variable indicating the block. The levels are the following:
99: warm-up block in ses-02_part-01
2: list A-plus (list A repeated on Day2)
3: list B. List that occurs after the first switch
4: List C . List that occurs after the second switch
5: List D List that occurs after the third switch


## *ses-02_part-02*

### participant
Participants' number

### recog_resp.keys
Response (left= old item, right = new item)

### recog_resp.rt
Reaction time for recognition

### conf_resp.keys
confidence ratings. Levels are the following: 
1 = Very unsure
2 = Unsure
3 = Sure
4 = Very Sure

### conf_resp.rt
Reaction time for confidence

### images
Image displayed on every trial

### corr_ans
Correct answer for every trial

### type
Variable indicating whether the image displayed is old or new




