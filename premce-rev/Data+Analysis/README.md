# Premce-Cat 
Description of the files and folders. 
## ambiguous_im
It contains the images that are ambiguous. 
## analysis_files
Files related to the different data analysis carried out. 
#### 01.analysis_day1.R
Analysis of the data of day1. 
#### 02.analysis_day2-contingencies.R
Analysis of the statistical learning task in day 2. 
#### 02.analysis_day2-recog.R
Analysis of the recognition test in day 2. 
#### 03.item_analysis.R
Item analysis
#### 04.analysis_day2-recog2.R
Analysis of recognition test in day 2, considering from different POV. 
#### 05.analysisRL.R
Analysis of the reinforcement learning model. 
## data_files
Folder containing the data after being renamed. 
## helper_functions
Folder containing functions used by the main scripts
#### cleanFiles.R
Function that extract the variables of interest from the raw data and print the new files in a folder data_files_clean that needs to be created first. 
#### cumAccbyChar.R
Function to create cumulative accuracy for each character separately. It is used to plot the accuracy by character. 
#### DataQualityCheck.R
Function that it was used by previous experiment versions to check for overall accuracy in statistical learning and recognition tasks. 
#### getCat.R
Function that take the key pressed by participant and returns the column where the category select can be found
#### selPart.R
Select the files that belongs to each of the three phases. 
## output_files
Data created by the several scripts
## raw_data
Raw data
#### ambiguousCat.txt
text files with the names of the images for which the categories where ambiguous. 
#### master_script.sh
Bash script that runs the other scripts. 