# PrEMCE. Protocol.

## ID: __________________________________

Day 1 |--------------------- | Day 2 | --------------------- | 
| ----- | ----- | ----- | ----- | 
Date: |	15/02/2021			| Date: | 	16/02/2021			
Starting time: | 13:00	| Starting time: | 		19:00
Ending time: | 13:22		| Ending time: |	19:53	

## Before Session on Day 1
### Documents available
- [x] Signed **Informed consent**.
- [x] Participant's **experiment links**.
- [x] Participant's **session protocol**.
- [x] Started a meeting in jitsi.

## During Session on Day 1
- [x] Ask about general well-being. Comments?: No
- [x] The participant is using a desktop computer or a laptop on a desk/table.
- [x] The participant is sitting down on a chair.
- [x] The participant is on a quiet room.
- [x] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [x] Yes
- [ ] No. Comments: The participant complained about...


### ses 01 - part 01
- [x] Send **ses 01 - part 01** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

- [x] At the end of the practice trials, invite participant to read the percentage. 
- [x] If performance is below 80%, provide explanation about the task again and invite participant to do the task again by pressing "j"
- [x] If participants performance is at least 80%, invite participant to press "n" to continue with the task, otherwise repeat previous point. 
- [x] After the practice trials, at the end of the first block ask participant to read the percentage on the screen. If satisfactory, invite participant to press "n" to continue, otherwise provide info and ask participant to press "j" to do the task again

Comments: Participant only managed to reach 40% during the task (although 80% during practice trials) and said that one of the monsters was especially hard to predict. She noted that the objects were presented so short that she had difficulties recognizing them. They also were difficult to categorize correctly.

## Before Session on Day 2
### Documents available
- [x] Participant's **experiment links**.
- [x] Participant's **session protocol**.
- [x] Started a meeting in jitsi.

## During Session on Day 2
- [x] Ask about general well-being. Comments?: No
- [x] The participant is using a desktop computer or a laptop on a desk/table.
- [x] The participant is sitting down on a chair.
- [x] The participant is on a quiet room.
- [x] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [s] Yes
- [ ] No. Comments:

### Phase 2
- [x] Send **ses 02 - part 01** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

### Phase 3
- [x] Send **ses 02 - part 02** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

Comments: Participant said that objects with much color or salient colors were easier to remember than objects who were rather colorless. She noted that she couldn't identify about 5 to 10 objects (especially machine-like objects). 
