# PrEMCE. Protocol.

## ID: 35

Day 1 |--------------------- | Day 2 | --------------------- | 
| ----- | ----- | ----- | ----- | 
Date: |	02.02.2022			| Date: | 				
Starting time: | 14:30	| Starting time: | 		
Ending time: | 15:30		| Ending time: |		

## Before Session on Day 1
### Documents available
- [x] Signed **Informed consent**.
- [x] Participant's **experiment links**.
- [x] Participant's **session protocol**.
- [x] Started a meeting in jitsi.

## During Session on Day 1
- [x] Ask about general well-being. Comments?:
- [x] The participant is using a desktop computer or a laptop on a desk/table.
- [x] The participant is sitting down on a chair.
- [x] The participant is on a quiet room.
- [x] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [x] Yes
- [ ] No. Comments: The participant complained about...


### ses 01 - part 01
- [x] Send **ses 01 - part 01** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

- [x] At the end of the practice trials, invite participant to read the percentage. 
- [x] If performance is below 80%, provide explanation about the task again and invite participant to do the task again by pressing "j"
- [] If participants performance is at least 80%, invite participant to press "n" to continue with the task, otherwise repeat previous point. 
- [] After the practice trials, at the end of the first block ask participant to read the percentage on the screen. If satisfactory, invite participant to press "n" to continue, otherwise provide info and ask participant to press "j" to do the task again

Comment: The paricipant could not reach more than 60% in the first block and wanted to quit the participation after 1 hour.



## Before Session on Day 2

 no further participation!

### Documents available
- [ ] Participant's **experiment links**.
- [ ] Participant's **session protocol**.
- [ ] Started a meeting in jitsi.

## During Session on Day 2
- [ ] Ask about general well-being. Comments?:
- [ ] The participant is using a desktop computer or a laptop on a desk/table.
- [ ] The participant is sitting down on a chair.
- [ ] The participant is on a quiet room.
- [ ] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [ ] Yes
- [ ] No. Comments:

### Phase 2
- [ ] Send **ses 02 - part 01** link.

Understood the task?
- [ ] Yes
- [ ] No. Comments:

### Phase 3
- [ ] Send **ses 02 - part 02** link.

Understood the task?
- [ ] Yes
- [ ] No. Comments:

