# PrEMCE. Protocol.

## ID: __________________________________

Day 1 |--------------------- | Day 2 | --------------------- | 
| ----- | ----- | ----- | ----- | 
Date: |	05/06/21			| Date: | 	06/06/21			
Starting time: | 12:30	| Starting time: | 		13:00
Ending time: | 12:57		| Ending time: |	14:16	

## Before Session on Day 1
### Documents available
- [x] Signed **Informed consent**.
- [x] Participant's **experiment links**.
- [x] Participant's **session protocol**.
- [x] Started a meeting in jitsi.

## During Session on Day 1
- [x] Ask about general well-being. Comments?:
- [x] The participant is using a desktop computer or a laptop on a desk/table.
- [x] The participant is sitting down on a chair.
- [x] The participant is on a quiet room.
- [x] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [x] Yes
- [ ] No. Comments: The participant complained about...


### ses 01 - part 01
- [x] Send **ses 01 - part 01** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

- [x] At the end of the practice trials, invite participant to read the percentage. 
- [ ] If performance is below 80%, provide explanation about the task again and invite participant to do the task again by pressing "j"
- [x] If participants performance is at least 80%, invite participant to press "n" to continue with the task, otherwise repeat previous point. 
- [x] After the practice trials, at the end of the first block ask participant to read the percentage on the screen. If satisfactory, invite participant to press "n" to continue, otherwise provide info and ask participant to press "j" to do the task again

## Before Session on Day 2
### Documents available
- [x] Participant's **experiment links**.
- [x] Participant's **session protocol**.
- [x] Started a meeting in jitsi.

## During Session on Day 2
- [x] Ask about general well-being. Comments?:
- [x] The participant is using a desktop computer or a laptop on a desk/table.
- [x] The participant is sitting down on a chair.
- [x] The participant is on a quiet room.
- [x] The participant is alone in the room.

In general, I would say that the participant is in an appropriate environment. 
- [x] Yes
- [ ] No. Comments:

### Phase 2
- [x] Send **ses 02 - part 01** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

Comments: Participant noted that the task was very long and exhausting. She said she needed a short break.

### Phase 3
- [x] Send **ses 02 - part 02** link.

Understood the task?
- [x] Yes
- [ ] No. Comments:

Comments: She noted that she couldn't recognize some objects or couldn't classify them confidently to one of the categories (referring to the task before)
