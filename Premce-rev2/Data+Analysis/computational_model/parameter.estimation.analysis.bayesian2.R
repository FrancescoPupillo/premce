#------------------------------------------------------------------------------#
# parameter estimation analysis
# "Wed Oct  6 18:00:33 2021"
#
#------------------------------------------------------------------------------#

rm(list=ls())

# retrieve data
est<-read.csv("output_files/RLdata_bayesian2.csv")

# delete list =0
est<-est[est$listN!=0,]
# parameters
param<-read.csv("output_files/estimated_parameters_bayesian2.csv")

# load the simulated data
# load simulated data
load("output_files/simulationDataBayesian2.Rdata")

# aggregate simperf at simulation level
simPerfAgg<-simPerf %>%
  group_by(hazardRate ,beta) %>%
  summarise( dev = sd(perOptim),
             perOptim = mean(perOptim) ,
             n = n(),
             se = dev/sqrt(n),
             upperSE = perOptim+se,
             lowerSE = perOptim-se)


# plot the graph with adding the data
ggplot(data = simPerfAgg, aes(x = hazardRate, y = beta, fill = perOptim))+
  geom_tile()+
  scale_fill_gradient(low="white", high="black",name ="p(correct \nchoice)")  +

  geom_point(data = param, aes(x=hazardRate, y = beta, fill=NULL))+
  
  theme_bw()

ggplot(data = estimates, aes(x=alpha, y = beta))+
  geom_point()

# get estimated parameter for each participant
est$hazardRate<-NA
est$beta<-NA
for (n in nrow(est)){
  est$hazardRate[n]<-param$hazardRate[param$PartNum==est$SubNum[n]]
  est$beta[n]<-param$beta[param$PartNum==est$SubNum[n]]
  
}

# summarise it first
Datawide<- est %>%
  group_by(  hazardRate, beta) %>%
  summarise(mean = mean(accuracy), sd = sd(accuracy)) 

# now create the before/after change point graphs
# AccData<-est  %>%
#   group_by(SubNum, hazardRate, beta,listN)  %>%
#   mutate(befAft=c(rep("afterCP", times=24), rep("beforeCP", times=24)))

est$befAft<-as.factor(est$befAft)

# check the learning rate
ggplot(est, aes(befAft, lr, color = befAft))+
  # geom_bar(aes(befAft, isoptimal, fill = befAft),
  #         position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  
  theme_bw()+
  #geom_line(data = groupData, aes(befAft, accuracy,group = SubNum), size=1, alpha=0.2, stat="summary")+
  
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+
  ylab("Learning Rate")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  #theme(axis.text.x = element_text( size = 20, angle = 45))+
  theme(axis.text.x = element_blank())

# runlength
ggplot(est, aes(befAft, expRLength, color = befAft))+
  # geom_bar(aes(befAft, isoptimal, fill = befAft),
  #         position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  
  theme_bw()+
  #geom_line(data = groupData, aes(befAft, accuracy,group = SubNum), size=1, alpha=0.2, stat="summary")+
  
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+
  ylab("ExpRunLength")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  #theme(axis.text.x = element_text( size = 20, angle = 45))+
  theme(axis.text.x = element_blank())

# uncertainty
ggplot(est, aes(befAft, uncertainty, color = befAft))+
  # geom_bar(aes(befAft, isoptimal, fill = befAft),
  #         position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  
  theme_bw()+
  #geom_line(data = groupData, aes(befAft, accuracy,group = SubNum), size=1, alpha=0.2, stat="summary")+
  
  facet_wrap(.~listN)+
  theme(strip.text.x = element_text(size = 13))+
  ylab("uncertainty")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  #theme(axis.text.x = element_text( size = 20, angle = 45))+
  theme(axis.text.x = element_blank())

