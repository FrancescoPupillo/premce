#------------------------------------------------------------------------------#
# parameter recovery function for feedback model
# Cerated: Mon Sep 27 18:12:03 2021"
#------------------------------------------------------------------------------#

rm(list=ls())

#library(foreach)
#library(doParallel)
# library(pracma)
# 
# setwd(dirname(getwd()))
# setwd(dirname(getwd()))


source("computational_model/simulation_functions/simulate_RescorlaWagner_feedbALL.R")
source(("computational_model/softmax.R"))
source(("computational_model/chooseMultinom.R"))
source(("computational_model/BICcompute.R"))

source("computational_model/likelihood_functions/lik_RescorlaWagner_feedbALL.R")
source("computational_model/fitting_functions/fit_RescorlaWagner_feedbALL.R")
source("helper_functions/getCat.R")
source("helper_functions/preSim.R")
source("helper_functions/getcorrCat.R")

# we need a structure of the task 
# load it

# let's start by practlist
listPath<-"lists/"
#listPath<-"/users/francesco/PowerFolders/Frankfurt_University/PREMCE/Pilots/Premce-rev2/list_creator/lists/"

practlist<-read.csv(paste0(listPath, "practlist.csv"))

# now the day1
day1list<-read.csv(paste0(listPath, "tasklistA1.csv"))

# now day2
day2list<-vector()
for (l in c("Aplus", "B", "C", "D")){
  
  currlist<-read.csv(paste0(listPath, "tasklist", l, "1", ".csv"))
  
  day2list<-rbind(day2list, currlist)
                     
}

# retrieve the arguments
Args<-commandArgs(trailingOnly = T) 
ID<-Args[1]

alpha<-as.numeric(Args[2])
# simulate data 
# how many simulations?
sims<-1

# how many trials per block?
trialsxblock<-48  # keeping the original

lengthparam<-10

# fitting patameters
#alphaseq<-seq(0, 1, length.out = lengthparam)

# beta limit
betalim<-10

# set seed
set.seed(12357)

# # beta is sampled from the exponential distribution
betaran<-sort(rexp(lengthparam, 3/betalim))

# detect cores for runnning in parallel
#cores=detectCores()

#cl <- makeCluster(floor(cores*3/4), outfile="") # to not overload your computer
#registerDoParallel(cl)

#data<-matrix(NA, nrow=lengthparam^2,ncol = 5)


pb<-txtProgressBar(min=0, max=length(betaran), style =3)

#dat<-foreach (j=1:sims, .combine=rbind,.packages=c('pracma'))  %dopar% {
  # counter
  #count<-1
  #for (a in 1:length(alphaseq)){
    # define the matrix

print(paste("starting loop, ID =", ID, "alpha =", alpha))
data<-matrix(NA, ncol = 5, nrow = length(betaran))

    for (b in 1:length(betaran)){
      
      #print(paste("started the ", count*j, "simulation over ", (lengthparam^2)*sims))
      
      # simulate data    
      sim<-simulate_RescorlaWagner_feedbALL(Data=day2list,alpha=alpha, beta = betaran[b],
                                          initialQ = 0.25)
      
      # create important variables
      #sim<-preSim(sim)
      
      # now fit the data
      est<-fit_RescorlaWagner_feedbALL(data=sim, alphaBound = c(0,1), 
                                     betaBound = c(1,40), initialQ =0.25)
      
      estAlpha<-est$alphabetaPAR[1]
      estBeta<-est$alphabetaPAR[2]
      
      data[b,]<-c(alpha, estAlpha, betaran[b], estBeta, est$BIC)
      
      #count <-count+1
      
      setTxtProgressBar(pb, b) 
      
      
    }
    
  #}

  #print (paste( "finished simulation N "), j)
  
print(paste("finished loop, ID =", ID, "alpha =", alpha))
      
  #data
  
#}
print( "checkpoint3")

df<-data.frame(data)

names(df)<-c("SimAlpha", "fitAlpha", "simBeta", "fitBeta", "BIC")

model<-"feedbALL"

print( "checkpoint4")

name<- paste("computational_model/cluster/output_files/parameterRecoveryRW", model, ".", "betalimit=",  betalim,  
             ".initialQ=", 0.25 ,"_ID=", ID, "_alpha=", alpha, sep="")

write.csv(df, paste0(name, ".csv"))

print( "checkpoint5")

stopCluster(cl)

print( "checkpoint6")

