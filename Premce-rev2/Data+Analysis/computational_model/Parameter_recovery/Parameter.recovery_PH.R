#------------------------------------------------------------------------------#
# parameter recovery function for Pearce Hall model - only value of the choice
# is updated in this version
#
# Cerated: Mon Sep 27 18:12:03 2021"
#------------------------------------------------------------------------------#

rm(list=ls())

library(foreach)
library(doParallel)
library(pracma)

setwd(dirname(getwd()))
setwd(dirname(getwd()))


source("computational_model/simulation_functions/simulate_PearceHall.R")
source(("computational_model/softmax.R"))
source(("computational_model/chooseMultinom.R"))
source(("computational_model/BICcompute.R"))
source("computational_model/likelihood_functions/lik_PearceHall.R")
source("computational_model/fitting_functions/fit_PearceHall.R")
source("helper_functions/getCat.R")
source("helper_functions/preSim.R")
source("helper_functions/getcorrCat.R")

# we need a structure of the task 
# load it

# let's start by practlist
listPath<-"lists/"
#listPath<-"/users/francesco/PowerFolders/Frankfurt_University/PREMCE/Pilots/Premce-rev2/list_creator/lists/"

practlist<-read.csv(paste0(listPath, "practlist.csv"))

# now the day1
day1list<-read.csv(paste0(listPath, "tasklistA1.csv"))

# now day2
day2list<-vector()
for (l in c("Aplus", "B", "C", "D")){
  
  currlist<-read.csv(paste0(listPath, "tasklist", l, "1", ".csv"))
  
  day2list<-rbind(day2list, currlist)
  
}


# simulate data 
# how many simulations?
sims<-3

lengthparam<-5

# fitting patameters
alphaseq<-seq(0, 1, length.out = lengthparam)

# beta limit
betalim<-10

# set seed
set.seed(12357)

# # beta is sampled from the exponential distribution
betaran<-sort(rexp(lengthparam, 3/betalim))

kseq<-seq(0, 1, length.out = lengthparam)

etaseq<-seq(0, 1, length.out = lengthparam)

# detect cores for runnning in parallel
cores=detectCores()

cl <- makeCluster(floor(cores*3/4), outfile="") # to not overload your computer
registerDoParallel(cl)

data<-matrix(NA, nrow=1,ncol = 9)

df<-data.frame(data)

names(df)<-c("simAlpha", "fitAlpha","simBeta", "fitBeta", 
             "simK", "fitK", "simEta", "fitEta", "BIC")

model<-"PH"

print( "checkpoint4")

name<- paste("output_files/parameterRecovery", model, ".", "betalimit=",  betalim,  
             ".initialQ=", 0.25 , sep="")

write.csv(df, paste0(name, ".csv"), row.names = F)

pb<-txtProgressBar(min=0, max=(lengthparam^2), style =3)

dat<-foreach (j=1:sims, .combine=rbind,.packages=c('pracma'))  %dopar% {
  
  print ("starting")
  # counter
  count<-1
  
  for (a in 1:length(alphaseq)){
    
    print ("a=", a)
    for (b in 1:length(betaran)){
      print ("b=", b)
      
      for (k in 1 : length(kseq)){
        print("k=", k)
        
        for (eta in 1 :length(kseq)){
          print("eta=", eta)
      
      #print(paste("started the ", count*j, "simulation over ", (lengthparam^2)*sims))
      
      # simulate data    
      sim<-simulate_PearceHall(Data=day2list,alpha_0=alphaseq[a], beta = betaran[b],
                                         k = kseq[k], eta = etaseq[eta], initialQ = 0.25)
      
      # create important variables
      #sim<-preSim(sim)
      
      #sim$
      
      # now fit the data
      est<-fit_PearceHall(data=sim, alphaBound = c(0,1),  betaBound = c(1,betalim),
                          kBound = c(0,1), etaBound = c(0,1), initialQ =0.25)
      
      estAlpha<-est$alphabetaPAR[1]
      
      estBeta<-est$alphabetaPAR[2]
      
      estk<-est$alphabetaPAR[3]
      
      esteta<-est$alphabetaPAR[4]
      
      data[1,]<-c(alphaseq[a], estAlpha, betaran[b], estBeta,
                      kseq[k], estk, etaseq[eta], esteta, est$BIC)
      
      count <-count+1
      
      setTxtProgressBar(pb, count) 
      
      temp<-read.csv( paste0(name, ".csv"))
      
      #append the data
      temp[nrow(temp)+1, ]<-c(alphaseq[a], estAlpha, betaran[b], estBeta,
                              kseq[k], estk, etaseq[eta], esteta, est$BIC)
      
      #write it
      write.csv(temp, paste0(name, ".csv"), row.names = F)
      

    }
    
      }
      
    }
    
  }
  
  data
  #print (paste( "finished simulation N "), j)
  
  
}

print( "checkpoint3")

df<-data.frame(dat)

names(df)<-c("simAlpha", "fitAlpha", "simBeta", "fitBeta",
            "simK", "fitk", "simEta", "fitEta", "BIC")

model<-"PH"

print( "checkpoint4")

name<- paste("output_files/parameterRecoveryRW", model, ".", "betalimit=",  betalim,  
             ".initialQ=", 0.25 , sep="")

write.csv(df, paste0(name, ".csv"))

print( "checkpoint5")

stopCluster(cl)

print( "checkpoint6")

