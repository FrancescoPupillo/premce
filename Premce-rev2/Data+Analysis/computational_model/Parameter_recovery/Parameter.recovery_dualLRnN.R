#------------------------------------------------------------------------------#
# parameter recovery function for dual LR model no N
# Cerated: Mon Sep 27 18:12:03 2021"
#------------------------------------------------------------------------------#

rm(list=ls())

#setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

library(foreach)
library(doParallel)
library(pracma)

setwd(dirname(getwd()))
setwd(dirname(getwd()))


source("computational_model/simulation_functions/simulate_dualLRwN.R")
source(("computational_model/softmax.R"))
source(("computational_model/chooseMultinom.R"))
source(("computational_model/BICcompute.R"))

source("computational_model/likelihood_functions/lik_dualLRwN.R")
source("computational_model/fitting_functions/fit_dualLRnN.R")
source("helper_functions/getCat.R")
source("helper_functions/preSim.R")
source("helper_functions/getcorrCat.R")

# we need a structure of the task 
# load it

# let's start by practlist
listPath<-"lists/"
#listPath<-"/users/francesco/PowerFolders/Frankfurt_University/PREMCE/Pilots/Premce-rev2/list_creator/lists/"

practlist<-read.csv(paste0(listPath, "practlist.csv"))

# now the day1
day1list<-read.csv(paste0(listPath, "tasklistA1.csv"))

# now day2
day2list<-vector()
for (l in c("Aplus", "B", "C", "D")){
  
  currlist<-read.csv(paste0(listPath, "tasklist", l, "1", ".csv"))
  
  day2list<-rbind(day2list, currlist)
  
}


# simulate data 
# how many simulations?
sims<-1

# how many trials per block?
trialsxblock<-48  # keeping the original

lengthparam<-5

# fitting patameters
alphafastseq<-seq(0, 1, length.out = lengthparam)

alphaslowseq<-seq(0, 1, length.out = lengthparam)

# beta limit
betalim<-10

# set seed
set.seed(12357)

# # beta is sampled from the exponential distribution
betaran<-sort(rexp(lengthparam, 3/betalim))

#Ns<-floor(seq(1,20, length.out=lengthparam))

# fixing N to 10
N<-10
# detect cores for runnning in parallel
cores=detectCores()

cl <- makeCluster(floor(cores*3/4), outfile="") # to not overload your computer
registerDoParallel(cl)

data<-matrix(NA, nrow=1,ncol = 7)
# print the file
df<-data.frame(data)

names(df)<-c("simAlphaFast", "fitAlphaFast","simAlphaSlow", "fitAlphaSlow", 
             "simBeta", "fitBeta", "BIC")

model<-"RLnN"

print( "checkpoint4")

name<- paste("output_files/parameterRecoveryRW", model, ".", "betalimit=",  betalim,  
             ".initialQ=", 0.25 , sep="")

write.csv(df, paste0(name, ".csv"), row.names = F)

pb<-txtProgressBar(min=0, max=(lengthparam^2), style =3)

#dat<-foreach (j=1:sims, .combine=rbind,.packages=c('pracma'))  %dopar% {
  # counter
  #count<-1
  #for (a in 1:length(alphafastseq)){
   dat<-foreach (a=1:length(alphafastseq), .combine=rbind,.packages=c('pracma'))  %dopar% {
    
    count<-1
     
    for (b in 1:length(alphaslowseq)){
      
      for (c in 1:length(betaran)){
        
        #for (n in 1:length(Ns)){
          
          print(paste("started the simulation a=",a,"b=", b, "c=", c))
          
          # simulate data    
          sim<-simulate_dualLRwN(Data=day2list,alphafast=alphafastseq[a], 
                                 alphaslow = alphaslowseq[b], 
                                 Beta = betaran[b], N = 10,
                                 initialQ = 0.25)
          
          # create important variables
          sim<-preSim(sim)
          
          # now fit the data
          est<-fit_dualLRnN(data=sim, alphaBound = c(0,1), 
                            betaBound = c(1,betalim), 
                            initialQ =0.25)
          
          estAlphafast<-est$alphabetaPAR[1]
          estAlphaslow<-est$alphabetaPAR[2]
          estBeta<-est$alphabetaPAR[3]
         # estN<-est$alphabetaPAR[4]
          

          data[1,]<-c(alphafastseq[a], estAlphafast, 
                          alphaslowseq[b],estAlphaslow,
                           betaran[c],estBeta, 
                           est$BIC)
          
          setTxtProgressBar(pb, count) 
          
          count <-count+1
          
          print(count)
          
          temp<-read.csv( paste0(name, ".csv"))
          
          #append the data
          temp[nrow(temp)+1, ]<-c(alphafastseq[a], estAlphafast, 
                                  alphaslowseq[b],estAlphaslow,
                                  betaran[c],estBeta, 
                                 est$BIC)
          
         #write it
          write.csv(temp, paste0(name, ".csv"), row.names = F)
          
         print(paste("printed a=", a, "b=", b, "c=", c))
        }
        
      }
      
    }
   # print (paste( "finished simulation N "), a)
    #data
    
  
  
  
  
  
  

  
#}

print( "checkpoint3")

save.image("param_recov_dualLRnN.RData")
# 
# df<-data.frame(dat)
# 
# names(df)<-c("simAlphaFast", "fitAlphaFast","simAlphaSlow", "fitAlphaSlow", 
#              "simBeta", "fitBeta", "sitN", "FitN", "BIC")
# 
# model<-"RLwN"
# 
# print( "checkpoint4")
# 
# name<- paste("output_files/parameterRecoveryRW", model, ".", "betalimit=",  betalim,  
#              ".initialQ=", 0.25 , sep="")
# 
# write.csv(df, paste0(name, ".csv"))

print( "checkpoint5")

stopCluster(cl)

print( "checkpoint6")

