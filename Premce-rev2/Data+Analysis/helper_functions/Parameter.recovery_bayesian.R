#------------------------------------------------------------------------------#
# parameter recovery function for bayesian model
# Cerated: Mon Sep 27 18:12:03 2021"
#------------------------------------------------------------------------------#

rm(list=ls())

library(foreach)
library(doParallel)
library(pracma)

source("computational_model/simulate_RescorlaWagner_obsALL.R")
source(("computational_model/softmax.R"))
source(("computational_model/chooseMultinom.R"))
source(("computational_model/BICcompute.R"))

source("computational_model/lik_RescorlaWagner_obsALL.R")
source("computational_model/fit_RescorlaWagner_obsALL.R")
source("helper_functions/getCat.R")
source("helper_functions/preSim.R")
source("helper_functions/getcorrCat.R")


# we need a structure of the task 
# load it

# let's start by practlist
listPath<-"/home/francesco/PowerFolders/Frankfurt_University/PREMCE/Pilots/Premce-rev2/list_creator/lists/"
#listPath<-"/users/francesco/PowerFolders/Frankfurt_University/PREMCE/Pilots/Premce-rev2/list_creator/lists/"

practlist<-read.csv(paste0(listPath, "practlist.csv"))

# now the day1
day1list<-read.csv(paste0(listPath, "tasklistA1.csv"))

# now day2
day2list<-vector()
for (l in c("Aplus", "B", "C", "D")){
  
  currlist<-read.csv(paste0(listPath, "tasklist", l, "1", ".csv"))
  
  day2list<-rbind(day2list, currlist)
  
}


# simulate data 
# how many simulations?
sims<-10

# how many trials per block?
trialsxblock<-48  # keeping the original

# fitting patameters
alphaseq<-seq(0, 1, length.out = 2)

# # beta is sampled from the exponential distribution
betaran<-sort(rexp(2,2/40))

# detect cores for runnning in parallel
cores=detectCores()

cl <- makeCluster(cores[1]-1, outfile="") # to not overload your computer
registerDoParallel(cl)

dat<-foreach (j=1:sims, .combine=rbind,.packages=c('pracma'))  %dopar% {
  # counter
  count<-1
  for (a in 1:length(alphaseq)){
    for (b in 1:length(betaran)){
      
      # simulate data    
      sim<-simulate_RescorlaWagner_obsALL(Data=day2list,alpha=alphaseq[a], beta = betaran[b],
                                          initialQ = 0.25)
      
      # create important variables
      sim<-preSim(sim)
      
      # now fit the data
      est<-fit_RescorlaWagner_obsAll(data=sim, alphaBound = c(0,1), 
                                     betaBound = c(1,40), initialQ =0.25)
      
      estAlpha<-est$alphabetaPAR[1]
      estBeta<-est$alphabetaPAR[2]
      
      data[count,]<-c(a, estAlpha, b, estBeta, est$BIC)
      
      count <-count+1
      
    }
  }
  
  data
}
