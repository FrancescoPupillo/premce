#------------------------------------------------------------------------------#
# Analysis optimal choice practice list and list A
# Cerated: "Wed Sep 29 10:15:54 2021"
#------------------------------------------------------------------------------#

rm(list=ls())

library(ggplot2)
library(dplyr)

# source helper functions
source("helper_functions/selPart.R")
source("helper_functions/getcorrCat.R")
source("helper_functions/perOpt.R")
source("helper_functions/getCat.R")
source("helper_functions/cumAcc.R")
source("helper_functions/cumAccbyChar.R")
source("helper_functions/indCP.R")
source("helper_functions/getOpt.R")
source("helper_functions/getCatresp.R")
source("helper_functions/isOpt.R")


# sel files
cd<-getwd()
setwd("data_files")
files<-selPart(1)
setwd(cd)

# delete the first two number three files
numb3<-files[substr(files, 1,2)=="03"][1:2]

files<-files[!files %in% numb3]

# # 2. calculate optimal choice
AccDay1pract<-vector()

for (j in 1 : (length(files))){
  tryCatch({
  day1<-read.csv(paste("data_files/", files[j], sep=""))
  
  # select practice
  task0<-day1[day1$switch_cond==0,]
  
  # exclude NAs
  task0<-task0[!is.na(task0$pract_resp.keys),]
  task0$SubNum<-rep(as.numeric(substr(files[j], 1,2)), times=nrow(task0))
  
  # report the character
  task0$character<-NA
  for (t in 1: nrow(task0)){
    task0$character[t]<-substr(task0$cuedCharacter[t], 9, 
                               (nchar(as.character(task0$cuedCharacter[t]))-4))
  }

  # get optimal choice
  task0<-getOpt(task0)
  
  task0<-getCatresp(task0)
  
  task0<-isOpt(task0)
  
  optchoice<-perOpt(task0)
  
  # bind to the previous participant
  AccDay1pract<-rbind(AccDay1pract, optchoice)
   
 
  },  error=function(e){cat("ERROR :",conditionMessage(e), "\n", "participant=", j)}) 

}

# plot the percentages
ggplot(AccDay1pract,aes( x = trialN, y = perOptimalChoice )) +
  geom_line() +
  facet_grid(.~SubNum,)

# plot the percentages by murkse
ggplot(AccDay1pract[!is.na(AccDay1pract$character),],aes( x = trialN, y = perOptimalChoiceByMurks, col=character )) +
  geom_line() +
  facet_grid(.~SubNum)

# now task1
# # 2. calculate cumulative accuracy per participant and reaction times
AccDay1task<-vector()

for (j in 1 : (length(files))){
  tryCatch({
    day1<-read.csv(paste("data_files/", files[j], sep=""))
    
    # select first list
    task1<-day1[day1$switch_cond==1,]
    
    # exclude NAs
    task1<-task1[!is.na(task1$task_resp.keys),]
    task1$SubNum<-rep(as.numeric(substr(files[j], 1,2)), times=nrow(task1))
    
    # report the character
    task1$character<-NA
    for (t in 1: nrow(task1)){
      task1$character[t]<-substr(task1$cuedCharacter[t], 9, 
                                 (nchar(as.character(task1$cuedCharacter[t]))-4))
    }
    
    # get optimal choice
    task1<-getOpt(task1)
    
    task1<-getCatresp(task1)
    
    task1<-isOpt(task1)
    
    optchoice<-perOpt(task1)
    
    # check if there are elements that are not present in either variables
    missingel<-setdiff(names(AccDay1task), names(optchoice))
    
    # create the missing elements in order to being able to bind the two
    for (elements in missingel){
      optchoice[[elements]]<-NA
    }
    
    # bind to the previous participant
    AccDay1task<-rbind(AccDay1task, optchoice)
    

    
  },  error=function(e){cat("ERROR :",conditionMessage(e), "\n", "participant=", j)}) 
  
}

# plot
# plot the percentages
ggplot(AccDay1task,aes( x = trialN, y = perOptimalChoice )) +
  geom_line() +
  theme_bw()+
  facet_wrap(.~SubNum)


  
  # subset the last 20 trials
  AccDay1tasksub<- AccDay1task%>%
                  filter(trialN>38 & trialN<49) %>%
                  group_by(participant) %>%
                  summarise(optchoice = mean(perOptimalChoice))
  
  View(AccDay1tasksub)
  
  
  # calculate the percentage optimal choice for the day 2
  # sel files
  cd<-getwd()
  setwd("data_files")
  files2<-selPart(2)
  setwd(cd)
  
  # delete the double files (3)
  numb3<-files2[substr(files2, 1,2)=="01"][1:2]
  
  files2<-files2[!files2 %in% numb3]
  
  # now task2
  AccDay2task<-vector()
  
  for (j in 1 : (length(files2))){
    tryCatch({
      day2<-read.csv(paste("data_files/", files2[j], sep=""))
      
      # select all but the first block, which is for refreshing contingencies
      task2<-day2[day2$switch_cond!=1,]
      
      # exclude NAs
      task2<-task2[!is.na(task2$switch_cond),]
      task2$SubNum<-rep(as.numeric(substr(files2[j], 1,2)), times=nrow(task2))
      
      # report the character
      task2$character<-NA
      for (t in 1: nrow(task2)){
      
          task2$character[t]<-substr(task2$cuedCharacter[t], 9, 
                                   (nchar(as.character(task2$cuedCharacter[t]))-4))
      }
      
      # get optimal choice
      task2<-getOpt(task2)
      
      task2<-getCatresp(task2)
      
      task2<-isOpt(task2)
  
      optchoice<-perOpt(task2)
      
      # check if there are elements that are not present in either variables
      missingel<-setdiff(names(AccDay2task), names(optchoice))
      
      # create the missing elements in order to being able to bind the two
      for (elements in missingel){
        optchoice[[elements]]<-NA
      }
      
      # bind to the previous participant
      AccDay2task<-rbind(AccDay2task, optchoice)
      
      
      
    },  error=function(e){cat("ERROR :",conditionMessage(e), "\n", "participant=", j)}) 
    
  }
  
  AccDay2task$switch_cond<-as.factor(AccDay2task$switch_cond)

  # plot it
  ggplot(AccDay2task, aes(x=trialN, y = perOptimalChoiceByMurks, color=character))+ geom_line()+
    geom_vline(xintercept = c(48, 96,144))+
    facet_wrap(.~SubNum)


  # create before and after
  
  AccDay2task<-AccDay2task  %>%
    group_by(SubNum, switch_cond)  %>%
    mutate(befAft=c(rep("afterCP", times=24), rep("beforeCP", times=24)))

  ggplot(AccDay2task[AccDay2task$SubNum==1,], aes(x= trialN, y = respCat, col = respCat))+
    geom_point()+
    facet_grid(SubNum~character)+   
    geom_vline(xintercept = c(48, 96,144))
  
  ggsave(paste0(cd, "/figures/plot_by_respCat.jpg"))
  
  ggplot(AccDay2task[AccDay2task$SubNum==1,], aes(x= trialN, y = perOptimalChoiceByMurks ))+
    geom_point(aes(col = respCat))+
    geom_line(alpha = 0.3)+
    facet_grid(character~.)+   
    geom_vline(xintercept = c(48, 96,144))+
    ylim(0,1)
  
  ggsave(paste0(cd, "/figures/plot_by_optChoice_byMurks.jpg"))
  
  ggplot(AccDay2task[AccDay2task$SubNum==1,], aes(x= trialN, y = perOptimalChoice ))+
    geom_point()+
    geom_line()+
    #facet_grid(character~.)+   
    geom_vline(xintercept = c(48, 96,144))+
    ylim(0,1)
  
  ggsave(paste0(cd, "/figures/perOptimalChoice.jpg"))
  

  ggplot(AccDay2task[AccDay2task$SubNum==1,], aes(x= trialN, y = perOptimalChoiceByMurks , col = respCat))+
    geom_point()+
    facet_grid(SubNum~character)+   
    geom_vline(xintercept = c(48, 96,144))
      
  #plot it as a function as before and after
  ggplot(AccDay2task, aes(befAft, isoptimal, color = befAft))+
   # geom_bar(aes(befAft, isoptimal, fill = befAft),
    #         position="dodge",stat="summary", fun.y="mean", SE=T)+
    stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
    #geom_jitter( size=1,width=0.1)+
    theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
    theme(axis.ticks.x = element_blank())+

    #theme_bw()+
    #geom_line(data = groupData, aes(befAft, accuracy,group = SubNum), size=1, alpha=0.2, stat="summary")+
    
    facet_grid(.~switch_cond)+
    theme(strip.text.x = element_text(size = 13))+ 
    ylab("PerOptimalChoice")+
    scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
    theme(axis.text.x = element_text( NULL))+
    theme(axis.title=element_text(size=14,face="bold"))+
    #theme(legend.position = "none")+
    ylim(0,1)
  
  
  # subset the last 20 trials
  AccDay2tasksub<-AccDay2task %>%
    group_by( befAft) %>%
    summarise(optchoice = mean(perOptimalChoice))
  
  View(AccDay2tasksub)
  # aggregate at the trial level
  # plot it
  ggplot(triallevel, aes(x=trial, y = mean))+ geom_line()+
    geom_vline(xintercept = c(48, 96,144))
    #facet_wrap(.~SubNum)

  plot(triallevel$mean)  
  