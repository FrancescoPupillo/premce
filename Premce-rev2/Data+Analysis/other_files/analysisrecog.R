# analysis day 3 recognition
rm(list=ls())
library(dplyr)
library(ggplot2)
library(lme4)
library(car)
library(gridExtra)
library(lmerTest)
source("helper_functions/selPart.R")
source("helper_functions/repeatAssoc.R")

cd<-getwd()
setwd("data_files")
files<-selPart(3)
setwd(cd)

# retrieve file
day2data<-read.csv("output_files/day2_accuracy.csv")

# modify file 1 as it seems to be linked to part 2 by mist
filetomod<-read.csv(paste("data_files/", files[1], sep=""))
filetomod$participant<-1
# save it
write.csv(filetomod,paste("data_files/", files[1], sep=""))

longDatarecog<-vector()
recogData<-vector()


longDatarecog<-vector()
recogData<-vector()
for (j in 1: length(files)){
  tryCatch({
    recog<-read.csv(paste("data_files/", files[j], sep=""))
    recog<-repeatAssoc(recog)
    # create a file with only the association task
    assoc<-recog[!is.na(recog$resp_assoc.rt),]
    # delete the association task in the recog file
    recog<-recog[!is.na(recog$recog_resp.rt), ]
    # select variables of interest
    recog <- select(recog,recog_resp.keys,recog_resp.rt,conf_resp.keys,conf_resp.rt,images,corr_ans, images)
    
    
    # compute accuracy
    recog$acc<-NA
    for (n in 1 :nrow(recog)){
      if (recog$recog_resp.keys[n]==recog$corr_ans[n]){
        recog$acc[n]<-1
      }else{recog$acc[n]<-0} 
    }
    
    # calculate hit, miss, rej, and FA
    for ( i in 1:nrow(recog)){
      if (recog$corr_ans[i]=="left"& recog$recog_resp.keys[i]=="left" ){
        recog$type[i]<-"HIT"
      } else if (recog$corr_ans[i]=="left"& recog$recog_resp.keys[i]=="right" ){
        recog$type[i]<-"Miss"
      } else if(recog$corr_ans[i]=="right"& recog$recog_resp.keys[i]=="right" ){
        recog$type[i]<-"CorrRej"
      } else if (recog$corr_ans[i]=="right"& recog$recog_resp.keys[i]=="left" ){
        recog$type[i]<-"FA"
      }
    }
    
    # now scoring for recognition each image
    # first subset the day2 data
    # for the subject number, the first file present "02" in the file name, while it is actyally
    # participant 1
    if (j==1){
      SubNum=1
    } else{
      SubNum<-as.numeric(substr(files[j], 1,2))
    }
    
    day2Sub<-day2data[day2data$SubNum==SubNum,]
    # delete task 0
    day2Sub<-day2Sub[day2Sub$listN!=0,]
    
    day2Sub$image<-as.character(day2Sub$image)
    
    # then substring the images in the actual file
    recog$imageName<-substr(recog$images, 17, (length(recog$images)-4))
    assoc$imageName<-substr(assoc$images, 17, (length(assoc$images)-4))
    
    
    # we are only testing some of the images presented in the statistical learning task,
    # the congruent ones. 
    # therefore, the approach should be to take only the congruent ones
    # take the accuracy of the recognition and add it to day2Sub
    
    day2Sub<-day2Sub[day2Sub$trialCond==1,]
    day2Sub$recogAcc<-NA
    day2Sub$confidence<-NA
    # do the same for association task
    day2Sub$association<-NA
    
    for (i in 1: nrow(day2Sub)){
      tryCatch({
        day2Sub$recogAcc[i]<-recog$acc[recog$imageName==day2Sub$image[i]]
        # add confidence
        day2Sub$confidence[i]<-recog$conf_resp.keys[recog$imageName==day2Sub$image[i]]
        # add association
        day2Sub$association[i]<-as.character(assoc$resp_assoc.keys[recog$imageName==day2Sub$image[i]])
        
      },error=function(w){print(paste("problem with part", j, "trial", i, "image",day2Sub$image[i]))} 
      )
    }
    
    longDatarecog<-rbind(longDatarecog, day2Sub)
    recog$SubNum<-SubNum
    recogData<-rbind(recogData, recog)
  })
}

# create variable for correct association
longDatarecog$corr_ans_assoc<-NA
for (n in 1:nrow(longDatarecog)){
  if (!is.na(longDatarecog$association[n])){
    
    if((longDatarecog$character[n]=="m2" & longDatarecog$association[n]=="left") |
       (longDatarecog$character[n]=="m5" & longDatarecog$association[n]=="right"))
    { longDatarecog$corr_ans_assoc[n]<-1}else{longDatarecog$corr_ans_assoc[n]<-0}
    
  }
}

# check how many times an image appears, to see if there are duplicates
count<- longDatarecog %>% 
  group_by(SubNum)%>%
  tally()

VoI<-c("SubNum", "type")
# wide dataset
wideData<-table(recogData[,VoI])

# convert it to a data.frame
wideData<-as.data.frame.matrix(wideData)

# include row names
wideData$SubNum<-rownames(wideData)

# reorder the columns to make the id variable the first one
wideData<-wideData[, c(5,1:4)]

# compute percentage HIT
wideData$HITrate<-wideData$HIT/(wideData$HIT+wideData$Miss)

# percentage false alarm
wideData$FArate<-wideData$FA/(wideData$FA+wideData$CorrRej)

# calculate dprime
library(psycho)

indices <- psycho::dprime(wideData$HIT, wideData$FA, wideData$Miss, wideData$CorrRej)
wideData<-cbind(wideData, indices)

# PR
wideData$hitMinFA<-wideData$HITrate-wideData$FArate

wideData

# recogn
longDatarecog %>%
  group_by(SubNum) %>%
  summarize(mean_assoc = mean(corr_ans_assoc, na.rm=T))


# convert the variable as factor
longDatarecog$befAft<-as.factor(longDatarecog$befAft)
levels(longDatarecog$befAft)<-c("afterCP", "beforeCP")

# before and after in recognition
# excluding the first list
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                                                position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_grid(.~rec_session)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  ylim(c(0,1))+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# by list
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                                                position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  ylim(c(0,1))+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# confidence
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, confidence))+ geom_bar(aes(befAft, confidence, fill = befAft),
                                                                                  position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_grid(accuracy~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab("Confidence")+
  ylim(c(0,4))+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# only for high confidence
ggplot(longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                                                      position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

summary(longDatarecog$befAft[longDatarecog$confidence>=3 ])
longDatarecog[is.na(longDatarecog$befAft)]

# is it significant?
befAftModCConf<-glmer(recogAcc~befAft+(1+befAft|SubNum), family=binomial,
                           data= longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2,])
summary(befAftModCConf)

# only for low confidence
ggplot(longDatarecog[longDatarecog$confidence<=2 & longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                                                      position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# check confidence before and after
ggplot(longDatarecog[ longDatarecog$listN!=2,], aes(befAft, confidence))+ geom_bar(aes(befAft, confidence, fill = befAft),
                                                                                   position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % confidence")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# check association before and after
ggplot(longDatarecog[ longDatarecog$listN!=2,], aes(befAft, corr_ans_assoc))+ 
  geom_bar(aes(befAft, corr_ans_assoc, fill = befAft),
                                    position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_grid(.~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % corr association")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# is it significant?
befAftAccModCondass<-glmer(corr_ans_assoc~befAft+(1+befAft|SubNum), family=binomial,
                           data= longDatarecog[longDatarecog$listN!=2,])
summary(befAftAccModCondass)

# association for high confidence
# check association before and after
ggplot(longDatarecog[ longDatarecog$listN!=2 & longDatarecog$confidence>=2,], aes(befAft, corr_ans_assoc))+ 
  geom_bar(aes(befAft, corr_ans_assoc, fill = befAft),
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_grid(.~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % corr association")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# association as a function of accfuracy
ggplot(longDatarecog[ longDatarecog$listN!=2 ,], aes(befAft, corr_ans_assoc))+ 
  geom_bar(aes(befAft, corr_ans_assoc, fill = befAft),
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % corr association")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# is it significant?
befAftAccModCondass<-glmer(corr_ans_assoc~befAft*accuracy+(1+befAft*accuracy|SubNum), family=binomial,
                           data= longDatarecog[longDatarecog$listN!=2,])
summary(befAftAccModCondass)

Anova(befAftAccModCondass)

# association as a function of accfuracy, by block
ggplot(longDatarecog[ longDatarecog$listN!=2 ,], aes(befAft, corr_ans_assoc))+ 
  geom_bar(aes(befAft, corr_ans_assoc, fill = befAft),
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(listN~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % corr association")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# check the switch back
longDatarecog$switch_back<-NA
for (n in 1:nrow(longDatarecog)){
  if (longDatarecog$listN[n]==4 & longDatarecog$character[n]=="m5"){
    longDatarecog$switch_back[n]<-1
  } else if (longDatarecog$listN[n]==5 & longDatarecog$character[n]=="m2"){
    longDatarecog$switch_back[n]<-1
  } else{
    longDatarecog$switch_back[n]<-0
  }
}

# select only no switch back
ggplot(longDatarecog[ longDatarecog$listN!=2 & longDatarecog$switch_back==1,], aes(befAft, recogAcc))+ 
  geom_bar(aes(befAft, recogAcc, fill = befAft),
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(listN~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % corr association")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")


# reaction times
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, RT))+ 
  geom_bar(aes(befAft, RT, fill = befAft),
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % RT")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# is it significant?
befAftAccModCondRT<-lmer(RT~befAft+(1+befAft|SubNum),  data= longDatarecog[longDatarecog$listN!=2,])
summary(befAftAccModCondRT)

# check the befaft level
summary(longDatarecog$befAft)

# check accuracy
summary(longDatarecog$recogAcc)



# as a function of accuracy
#by accuracy
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                                                position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_grid(.~rec_session)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")+
  facet_wrap(.~accuracy)


# analyse
befAftAccModCondacc<-glmer(recogAcc~befAft*accuracy+(1+befAft|SubNum), family=binomial, data= longDatarecog[longDatarecog$listN!=2,])
summary(befAftAccModCondacc)

# only incorrect
befAftAccModCondacc<-glmer(recogAcc~befAft+(1+befAft|SubNum), family=binomial, 
                           data= longDatarecog[longDatarecog$listN!=2 & longDatarecog$accuracy==0,])
summary(befAftAccModCondacc)

# correct
befAftAccModCondacc<-glmer(recogAcc~befAft+(1+befAft|SubNum), family=binomial, 
                           data= longDatarecog[longDatarecog$listN!=2 & longDatarecog$accuracy==1,])
summary(befAftAccModCondacc)

# only for high confidence
ggplot(longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                                                      position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# only for low confidence
ggplot(longDatarecog[longDatarecog$confidence<3 & longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                                                                              position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")



# by block
ggplot(longDatarecog[ longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                                                                             position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(listN~accuracy)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# condition and 