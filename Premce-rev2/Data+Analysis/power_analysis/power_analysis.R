#------------------------------------------------------------------------------#
# Power analysis for premce data
# "Wed Oct 20 22:07:16 2021"
#------------------------------------------------------------------------------#
rm(list=ls())

library(dplyr)
library(ggplot2)
library(lme4)
library(car)
library(gridExtra)
library(lmerTest)
library(lme4)
library(mixedpower)
library(languageR)

# load data
data<-read.csv( "output_files/recognitionData.csv")


# -------------------------------------------------------


# --------------------------------------------------------------------------- #
# 1. LOAD DATA AND FIT MODEL: 
# --------------------------------------------------------------------------- #

# just run the next couple of lines - nothing to do for us at this point. 


model<-glmer(recogAcc~befAft+(1+befAft | SubNum), family = binomial,
             data = data[data$listN!=2,])

betas <- model@beta # the order corresponds to the order the effects are listed in the model summary

summary(model)

fixed_effects<-"befAft"

# now we can change those values! let's make them 15% smaller! (but feel free to also change them otherwise)
betas_smaller <- betas*0.85

simvar <- "SubNum" # which random factor do we want to vary?
steps <- c(40,50,  60, 70,80) 

# default in mixedpower() is SESOI = F and will not include a SESOI in the simulation. Assigns your betas_smaller 
# to SESOI to include it in the simulation
SESOI <- F 

# ------------------------------------------------- #
# run:
(power <- mixedpower(model = model, data = data[data$listN!=2,],
                     fixed_effects = fixed_effects,
                     simvar = simvar, steps = steps,
                     critical_value = 2, n_sim = 25, # let's not kill your laptop.. 
                     SESOI = SESOI))                 #  if it is still taking too long feel free to change this to 20 or 30
#                                                     -> but your "real" simulation should include 1000 runs!
# ---------------------------------high confidence

modelHC<-glmer(recogAcc~befAft+(1+befAft | SubNum), family = binomial,
               data = data[data$listN!=2 & data$confidence>2,])

summary(modelHC)

steps <- c(20,25, 30, 40 ) 

(powerHC <- mixedpower(model = modelHC, data = data[data$listN!=2 &data$confidence>2,],
                     fixed_effects = fixed_effects,
                     simvar = simvar, steps = steps,
                     critical_value = 2, n_sim = 25, # let's not kill your laptop.. 
                     SESOI = SESOI))                 #  if it is still taking too long feel free to change this to 20 or 30
# reduce beta by 15%

betasHC <- modelHC@beta # the order corresponds to the order the effects are listed in the model summary

betas_smaller <- betasHC*0.85

(powerHCred <- mixedpower(model = modelHC, data = data[data$listN!=2 &data$confidence>2,],
                       fixed_effects = fixed_effects,
                       simvar = simvar, steps = steps,
                       critical_value = 2, n_sim = 25, # let's not kill your laptop.. 
                       SESOI = SESOI))  

# ---------------------------------confidence weighting
source("helper_functions/getConfWeight.R")

dataCW<-getConfWeight(data)

modelCW<-glmer(recogAcc~befAft+(1+befAft | SubNum), family = binomial,
               data = dataCW[dataCW$listN!=2, ])

steps <- c(30,40, 50, 60 , 70) 


(powerCW <- mixedpower(model = modelCW, data = dataCW[dataCW$listN!=2,],
                       fixed_effects = fixed_effects,
                       simvar = simvar, steps = steps,
                       critical_value = 2, n_sim = 50, # let's not kill your laptop.. 
                       SESOI = SESOI))  

# reduce beta by 15%

betasCW <- modelCW@beta # the order corresponds to the order the effects are listed in the model summary

betas_smaller <- betasCW*0.85

(powerCWred <- mixedpower(model = modelCW, data = dataCW[dataCW$listN!=2,],
                          fixed_effects = fixed_effects,
                          simvar = simvar, steps = steps,
                          critical_value = 2, n_sim = 50, # let's not kill your laptop.. 
                          SESOI = betas_smaller))  


#-----------------------------------------------------------------------------#

# with accyracy
#-----------------------------------------------------------------------------#
modelACC<-glmer(recogAcc~befAft+accuracy+(1+befAft+accuracy | SubNum), family = binomial,
             data = data[data$listN!=2,])


betasACC <- modelACC@beta # the order corresponds to the order the effects are listed in the model summary

summary(modelACC)

fixed_effects<-"befAft"

# now we can change those values! let's make them 15% smaller! (but feel free to also change them otherwise)
betas_smallerACC <- betasACC*0.85


# now decrease the beta
(powerACC <- mixedpower(model = modelACC, data = data[data$listN!=2,],
                     fixed_effects = fixed_effects,
                     simvar = simvar, steps = steps,
                     critical_value = 2, n_sim = 50, # let's not kill your laptop.. 
                     SESOI = SESOI)) 

(powerACCred <- mixedpower(model = modelACC, data = data[data$listN!=2,],
                        fixed_effects = fixed_effects,
                        simvar = simvar, steps = steps,
                        critical_value = 2, n_sim = 50, # let's not kill your laptop.. 
                        SESOI = betas_smallerACC, database =F)) 

# save the results of the simulations
save.image("power_analysis/power_analysis_res.Rdata")

