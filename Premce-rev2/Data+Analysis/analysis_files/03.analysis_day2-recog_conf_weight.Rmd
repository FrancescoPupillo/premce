---
title: "Untitled"
author: "Francesco Pupillo"
date: "4/15/2021"
output:
   html_document:
    toc: true
    toc_depth: 2
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r message=FALSE, warning=FALSE, include=T}
# set current directory to the parent folder
setwd(dirname(getwd()))

rm(list=ls())
library(dplyr)
library(ggplot2)
library(lme4)
library(car)
library(gridExtra)
library(lmerTest)
library(viridis)

source("helper_functions/selPart.R")
source("helper_functions/repeatAssoc.R")

```


```{r, include=T}
setwd(dirname(getwd()))

# set current directory to the parent folder
cd<-getwd()
setwd("data_files")
files<-selPart(3)
setwd(cd)

# retrieve file
day2data<-read.csv("output_files/day2_accuracy.csv")

# modify file 1 as it seems to be linked to part 2 by mist. So the part 2 file actually belogs to participant 1.
filetomod<-read.csv(paste("data_files/", files[1], sep=""))
filetomod$participant<-1
# save it
write.csv(filetomod,paste("data_files/", files[1], sep=""))
```

```{r include=F}
setwd(dirname(getwd()))

longDatarecog<-vector()
recogData<-vector()


longDatarecog<-vector()
recogData<-vector()


for (j in 1: length(files)){
  tryCatch({


    recog<-read.csv(paste("data_files/", files[j], sep=""))
    recog<-repeatAssoc(recog)
    # create a file with only the association task
    assoc<-recog[!is.na(recog$resp_assoc.rt),]
    # delete the association task in the recog file
    recog<-recog[!is.na(recog$recog_resp.rt), ]
    # select variables of interest
    recog <- dplyr::select(recog,recog_resp.keys,recog_resp.rt,conf_resp.keys,conf_resp.rt,images,corr_ans, images)
    
    # get SubNum
    SubNum<-unique(recog$participant)
    
    # check if there are repeated images for that participant
    day2Sub<-day2data[day2data$SubNum==SubNum,]
    # selet only file for no warmup
    day2Sub<-day2Sub[day2Sub$listN!=0,]
    
    # check if there are repetition
    objects<-as.character(unique(day2Sub$image))
    
    repIm<-vector()
    counter<-1
    for (n in 1:length(objects)){
      obj<-day2Sub$trialNum[day2Sub$image==objects[n]]
      if (length(obj)>1){
        repIm[counter]<-objects[n]
        counter<-counter+1
      }
      
    }
    
    if (length(repIm)>1){print(paste("repeated object for Sub =", SubNum))}
    
    # compute accuracy
    recog$acc<-NA
    for (n in 1 :nrow(recog)){
      if (recog$recog_resp.keys[n]==recog$corr_ans[n]){
        recog$acc[n]<-1
      }else{recog$acc[n]<-0} 
    }
    
    # calculate hit, miss, rej, and FA
    for ( i in 1:nrow(recog)){
      if (recog$corr_ans[i]=="left"& recog$recog_resp.keys[i]=="left" ){
        recog$type[i]<-"HIT"
      } else if (recog$corr_ans[i]=="left"& recog$recog_resp.keys[i]=="right" ){
        recog$type[i]<-"Miss"
      } else if(recog$corr_ans[i]=="right"& recog$recog_resp.keys[i]=="right" ){
        recog$type[i]<-"CorrRej"
      } else if (recog$corr_ans[i]=="right"& recog$recog_resp.keys[i]=="left" ){
        recog$type[i]<-"FA"
      }
    }
    
    # now scoring for recognition each image
    # first subset the day2 data
    # for the subject number, the first file present "02" in the file name, while it is actyally
    # participant 1
    if (j==1){
      SubNum=1
    } else{
      SubNum<-as.numeric(substr(files[j], 1,2))
    }
    
    day2Sub<-day2data[day2data$SubNum==SubNum,]
    # delete task 0
    day2Sub<-day2Sub[day2Sub$listN!=0,]
    
    day2Sub$image<-as.character(day2Sub$image)
    
    # then substring the images in the actual file
    recog$imageName<-substr(recog$images, 17, (length(recog$images)-4))
    assoc$imageName<-substr(assoc$images, 17, (length(assoc$images)-4))
    
    # we are only testing some of the images presented in the statistical learning task,
    # the congruent ones. 
    # therefore, the approach should be to take only the congruent ones
    # take the accuracy of the recognition and add it to day2Sub
    
    day2Sub<-day2Sub[day2Sub$trialCond==1,]
    day2Sub$recogAcc<-NA
    day2Sub$confidence<-NA
    
    # do the same for association task
    day2Sub$association<-NA
    
    for (i in 1: nrow(day2Sub)){
      tryCatch({
        day2Sub$recogAcc[i]<-recog$acc[recog$imageName==day2Sub$image[i]]
        # add confidence
        day2Sub$confidence[i]<-recog$conf_resp.keys[recog$imageName==day2Sub$image[i]]
        # add association
        day2Sub$association[i]<-as.character(assoc$resp_assoc.keys[recog$imageName==day2Sub$image[i]])
        # RT
        day2Sub$RTrecog[i]<-recog$recog_resp.rt[recog$imageName==day2Sub$image[i]]

      },error=function(w){print(paste("problem with part", j, "trial", i, "image",day2Sub$image[i]))} 
      )
    }
    
    longDatarecog<-rbind(longDatarecog, day2Sub)
    recog$SubNum<-SubNum
    recogData<-rbind(recogData, recog)
  })
}

# create variable for correct association
longDatarecog$corr_ans_assoc<-NA
for (n in 1:nrow(longDatarecog)){
  if (!is.na(longDatarecog$association[n])){
    
    if((longDatarecog$character[n]=="m2" & longDatarecog$association[n]=="left") |
       (longDatarecog$character[n]=="m5" & longDatarecog$association[n]=="right"))
    { longDatarecog$corr_ans_assoc[n]<-1}else{longDatarecog$corr_ans_assoc[n]<-0}
    
  }
}

# check how many times an image appears, to see if there are duplicates
count<- longDatarecog %>% 
  group_by(SubNum)%>%
  tally()


# write the file
write.csv(longDatarecog, "output_files/recognitionData.csv", col.names = F)

```

```{r}
setwd(dirname(getwd()))

# get the dprime null distribution
source("helper_functions/dprime_thres.R")

setwd(dirname(getwd()))
# create the threshold for excluding participants through a permutation analysis
permutations <-dprime_thres(144, 144, 5000, 0.95)
thres<-permutations$thres

null_distr<-permutations$distribution
null_distr<-as.data.frame(null_distr)
null_distr$SubNum<-1:nrow(null_distr)
null_distr$SubNum<-null_distr$SubNum+200 # to distinguish with the actual partic
names(null_distr)[1]<-"dprime"
null_distr$type<-"Null"


```


```{r general performance, echo=FALSE}
setwd(dirname(getwd()))

VoI<-c("SubNum", "type")
# wide dataset
wideData<-table(recogData[,VoI])

# convert it to a data.frame
wideData<-as.data.frame.matrix(wideData)

# include row names
wideData$SubNum<-rownames(wideData)

# reorder the columns to make the id variable the first one
wideData<-wideData[, c(5,1:4)]

# compute percentage HIT
wideData$HITrate<-wideData$HIT/(wideData$HIT+wideData$Miss)

# percentage false alarm
wideData$FArate<-wideData$FA/(wideData$FA+wideData$CorrRej)

# calculate dprime
library(psycho)

indices <- psycho::dprime(wideData$HIT, wideData$FA, wideData$Miss, wideData$CorrRej)
wideData<-cbind(wideData, indices$dprime)

# PR
wideData$hitMinFA<-wideData$HITrate-wideData$FArate

wideData

# take only subnum and dprime
SDT<-wideData[, c("SubNum", "indices$dprime")]
names(SDT)[2]<-"dprime"

SDT$type<-"Empirical"

# merge them
distr_all<-rbind(null_distr, SDT)

# plot 
PlotBind<-ggplot(distr_all, aes(x= dprime, fill=type))

#library(plyr)
PlotBind+
  #geom_histogram(aes(y = ..density..),
     #            colour = c(1,2), fill = "white") +
  geom_density(alpha = .5)+
  theme_bw()+
  geom_boxplot()+
  geom_vline(xintercept = thres)+
  theme(
    plot.title = element_text(size = 22),
    axis.title.x = element_text(size = 20),
    axis.title.y = element_text(size = 20),
    axis.text=element_text(size=20)
  )+
  xlab("d'")+
  theme(legend.position = "none")+
 # ggtitle("Null vs)+
  scale_fill_viridis(discrete=TRUE, option = "magma") 

# recogn
assocMem<-longDatarecog[longDatarecog$recogAcc==1,] %>% # only for accurate performance
  group_by(SubNum) %>%
  summarize(mean_assoc = mean(corr_ans_assoc, na.rm=T))

```
# Confidence
```{r}




# now calculate the overall performance only for high confidence
VoI<-c("SubNum", "type")
# wide dataset
wideData<-table(recogData[recogData$conf_resp.keys>=3,VoI])

# convert it to a data.frame
wideData<-as.data.frame.matrix(wideData)

# include row names
wideData$SubNum<-rownames(wideData)

# reorder the columns to make the id variable the first one
wideData<-wideData[, c(5,1:4)]

# compute percentage HIT
wideData$HITrate<-wideData$HIT/(wideData$HIT+wideData$Miss)

# percentage false alarm
wideData$FArate<-wideData$FA/(wideData$FA+wideData$CorrRej)

# calculate dprime
library(psycho)

indices <- psycho::dprime(wideData$HIT, wideData$FA, wideData$Miss, wideData$CorrRej)
wideData<-cbind(wideData, indices$dprime)

# PR
wideData$hitMinFA<-wideData$HITrate-wideData$FArate


overAcc<-wideData %>%
     dplyr::summarise(Hitsagg =  mean(HIT),
                      HITsd = sd (HIT), 
                      FAagg= mean(FA), 
                      FAsd = sd(FA, na.rm=T), 
                      HITrateagg = mean(HITrate), 
                      HITratesd = sd(HITrate), 
                      FArateagg = mean(FArate), 
                      FAratesdagg = sd(FArate), 
                      dprimeagg = mean(indices$dprime), 
                      dprimesd = sd(indices$dprime),
                      hitMinFAagg = mean(hitMinFA),
                      hitMinFAsd = sd(hitMinFA))


```

# confidence

```{r plots}
setwd(dirname(getwd()))

# convert the variable as factor
longDatarecog$befAft<-as.factor(longDatarecog$befAft)
levels(longDatarecog$befAft)<-c("afterCP", "beforeCP")

# is confidence influenced by change point?
setwd(dirname(getwd()))


befAftConfidence<-lmer(confidence~befAft+(1+befAft|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])

summary(befAftConfidence)

# let's create confidence - weighted accuracy
# create confidence weighted
longDatarecog$con_weighted_acc = 
        ifelse(longDatarecog$recogAcc == 1, longDatarecog$confidence + 4, 
            ifelse(longDatarecog$recogAcc == 0, 
                ifelse(longDatarecog$confidence == 1, 4, 
                     ifelse(longDatarecog$confidence == 2, 3, 
                        ifelse(longDatarecog$confidence == 3 , 2, 1))), "NA"))

longDatarecog$con_weighted_acc<-as.numeric(as.character(longDatarecog$con_weighted_acc))

# count how many trials
count<- longDatarecog[longDatarecog$listN!=2,]%>%
        group_by(SubNum, befAft) %>%
        tally()

# check average performance
longDatarecog[longDatarecog$listN!=2,]%>%
        summarise(con_weighted_acc = mean(con_weighted_acc, na.rm=T), 
                  stdev = sd(con_weighted_acc, na.rm = T)) 

sd(longDatarecog$con_weighted_acc[longDatarecog$listN!=2], na.rm = T)

# rename accuracy
longDatarecog$accuracy<-as.factor(longDatarecog$accuracy)
levels(longDatarecog$accuracy)<-c("Incorrect", "Correct")

# group by accuracy
groupDataAcc<-longDatarecog[ longDatarecog$listN!=2,] %>%
             group_by(SubNum, accuracy, listN) %>%
  dplyr::summarise (con_weighted_acc = mean(con_weighted_acc))

# group by bef Aft
groupData<-longDatarecog[longDatarecog$listN!=2,] %>%
             group_by(SubNum, befAft, listN) %>%
  dplyr::summarise (
             RT=mean(RT), 
             con_weighted_acc = mean(con_weighted_acc)) 

# plot accuracy
ggplot(longDatarecog[longDatarecog$listN!=2 ,], aes(accuracy, con_weighted_acc))+ 
  geom_bar(aes(accuracy, con_weighted_acc, fill = accuracy), 
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 1, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  geom_jitter(data = groupDataAcc, aes(accuracy, con_weighted_acc,group = SubNum), size=1,
              width = 0.1, alpha=0.2, stat="summary")+
  geom_point(data = groupDataAcc,aes(group = SubNum),
             #cex = 3.25,
             alpha=0.5,
             position = position_dodge(0.1),
             show.legend = FALSE,
             stat="summary")+
  theme_bw()+
  #facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab("Confidence-weighted Accuracy")+
  coord_cartesian(ylim=c(4,8))+  # xlab("feedback")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("Incorrect" = "dodgerblue3", "Correct" = "darkgreen"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

groupDataAccMean<-longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2
                     & !is.na(longDatarecog$recogAcc),] %>%
             group_by(SubNum, accuracy, listN) %>%
  dplyr::summarise (con_weighted_acc = mean(con_weighted_acc))

# before and after in confidence_weighted accuracy
# excluding the first list
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, con_weighted_acc))+
  geom_bar(aes(befAft, con_weighted_acc, fill = befAft), position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  geom_jitter(data = groupData, aes(befAft, con_weighted_acc,group = SubNum), size=1,
              width = 0.1, alpha=0.2, stat="summary")+

  #facet_grid(.~rec_session)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  coord_cartesian(ylim=c(4,8))+  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")


# analyse
# try the random effects
baselineMod<-lmer(con_weighted_acc~befAft*accuracy*listN+
                          (1|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])


# add random effect for accuracy
baselineModRAcc<-lmer(con_weighted_acc~befAft*accuracy*listN+
                          (1+accuracy|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])

anova(baselineMod, baselineModRAcc)
# no inrease in fit


# change point
baselineModRbefAft<-lmer(con_weighted_acc~befAft*accuracy*listN+
                          (1+befAft|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])
 anova(baselineMod, baselineModRbefAft)


# that is siginifcane
# now the change point


baselineModRAcc<-lmer(con_weighted_acc~befAft*accuracy*listN+
                          (1+accuracy|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])


# confidence weighted with list
befAftConfWAccMod<-lmer(con_weighted_acc~befAft*accuracy*listN+
                          (1+befAft*accuracy*listN|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])
summary(befAftConfWAccMod)

Anova(befAftConfWAccMod)
# non significant, so we can leave accuracy out
befAftConfW<-lmer(con_weighted_acc~befAft+
                          (1+befAft|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])
summary(befAftConfW)

befAftConfWAccModNoBlock<-lmer(con_weighted_acc~befAft*accuracy+(1+befAft*accuracy|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])
summary(befAftConfWAccModNoBlock)

Anova(befAftConfWAccModNoBlock, type = "III")

# without accuracy
befAftConfWModNoInt<-lmer(con_weighted_acc~befAft+accuracy+(1|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])

summary(befAftConfWModNoInt)

befAftConfWModNoInt<-lmer(con_weighted_acc~befAft+accuracy+(1|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])

summary(befAftConfWModNoInt)


ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, con_weighted_acc))+ geom_bar(aes(befAft, con_weighted_acc, fill = befAft),
                                                                                position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  geom_line(data = groupData, aes(befAft, con_weighted_acc,group = SubNum), size=1, alpha=0.2, stat="summary")+

  #facet_grid(.~rec_session)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
coord_cartesian(ylim=c(4,8))+  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")



# by list
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                       position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  ylim(c(0,1))+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20, angle = 15))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# confidence
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, confidence))+ geom_bar(aes(befAft, confidence, fill = befAft),
                                                                                  position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_grid(accuracy~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab("Confidence")+
  ylim(c(0,4))+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20, angle = 45))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")




# only for high confidence
groupDataHC<-longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2
                     & !is.na(longDatarecog$recogAcc),] %>%
             group_by(SubNum, befAft, listN) %>%
  dplyr::summarise (recogAcc = mean(recogAcc), 
             RT=mean(RT), 
             con_weighted_acc = mean(con_weighted_acc)) 

ggplot(longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2
                     & !is.na(longDatarecog$recogAcc),], aes(befAft, recogAcc))+ 
  geom_bar(aes(befAft, recogAcc, fill = befAft), 
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 1, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  geom_line(data = groupDataHC, aes(befAft, recogAcc,group = SubNum), size=1, alpha=0.2, stat="summary")+
  geom_point(data = groupDataHC,aes(group = SubNum),
             #cex = 3.25,
             alpha=0.5,
             position = position_dodge(0.1),
             show.legend = FALSE,
             stat="summary")+
  theme_bw()+
  #facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  coord_cartesian(ylim=c(0.5,1))+  # xlab("feedback")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")
  #scale_fill_viridis_d()

# now by list N
ggplot(longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2
                     & !is.na(longDatarecog$recogAcc),], aes(befAft, recogAcc))+ 
  geom_bar(aes(befAft, recogAcc, fill = befAft), 
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 1, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  geom_line(data = groupDataHC, aes(befAft, recogAcc,group = SubNum), size=1, alpha=0.2, stat="summary")+
  geom_point(data = groupDataHC,aes(group = SubNum),
             #cex = 3.25,
             alpha=0.5,
             position = position_dodge(0.1),
             show.legend = FALSE,
             stat="summary")+
  theme_bw()+
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  coord_cartesian(ylim=c(0.5,1))+  # xlab("feedback")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# analyze accuracy
# group by ID


longDatarecog$accuracy<-as.factor(longDatarecog$accuracy)

levels(longDatarecog$accuracy)<-c("Incorrect", "Correct")

groupDataAcc<-longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2
                     & !is.na(longDatarecog$recogAcc),] %>%
             group_by(SubNum, accuracy, listN) %>%
  dplyr::summarise (recogAcc = mean(recogAcc))


# without grouping


ggplot(longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2
                     & !is.na(longDatarecog$recogAcc),], aes(accuracy, recogAcc))+ 
  geom_bar(aes(accuracy, recogAcc, fill = accuracy), 
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 1, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  geom_line(data = groupDataAcc, aes(accuracy, recogAcc,group = SubNum), size=1, alpha=0.2, stat="summary")+
  geom_point(data = groupDataAcc,aes(group = SubNum),
             #cex = 3.25,
             alpha=0.5,
             position = position_dodge(0.1),
             show.legend = FALSE,
             stat="summary")+
  theme_bw()+
  #facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  coord_cartesian(ylim=c(0.5,1))+  # xlab("feedback")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("Incorrect" = "dodgerblue3", "Correct" = "darkgreen"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# by block
ggplot(longDatarecog[longDatarecog$confidence>=3 & longDatarecog$listN!=2
                     & !is.na(longDatarecog$recogAcc),], aes(accuracy, recogAcc))+ 
  geom_bar(aes(accuracy, recogAcc, fill = accuracy), 
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 1, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  geom_line(data = groupDataAcc, aes(accuracy, recogAcc,group = SubNum), size=1, alpha=0.2, stat="summary")+
  geom_point(data = groupDataAcc,aes(group = SubNum),
             #cex = 3.25,
             alpha=0.5,
             position = position_dodge(0.1),
             show.legend = FALSE,
             stat="summary")+
  theme_bw()+
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  coord_cartesian(ylim=c(0.5,1))+  # xlab("feedback")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("Incorrect" = "dodgerblue3", "Correct" = "darkgreen"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# only for low confidence
ggplot(longDatarecog[longDatarecog$confidence<=2 ,], aes(befAft, accuracy))+
  geom_bar(aes(befAft, recogAcc, fill = befAft), position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20, angle = 45))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")



# now cool graphs
ggplot(groupDataHC, aes(befAft, recogAcc))+
  geom_point(aes(group = SubNum, color = befAft),
             cex = 3.25,
             
             position = position_dodge(0.1),
             show.legend = FALSE,
             stat="summary")+
  geom_boxplot(alpha = 0)+
    stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+

  geom_line(data = groupDataHC, aes(befAft, recogAcc,group = SubNum), size=1, alpha=0.2, stat="summary")+
  theme_classic() +
  scale_color_manual( values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred")) +
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  
  theme(strip.text.x = element_text(size = 13))+
  ylab("Accuracy")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+

  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")



# accuracy
ggplot(longDatarecog[longDatarecog$confidence>=32 ,], aes(befAft, accuracy))+ geom_bar(aes(befAft, accuracy, fill = befAft),
                                                                                      position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20, angle = 45))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

befAftConfAccMod<-glmer(accuracy~befAft+(1+befAft|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])
summary(a)
# is that significant?
# first, take into account all 
# is that significant?

longDatarecog$befAft.c <- ifelse(longDatarecog$befAft == "afterCP", 0.5, -0.5)

longDatarecog$accuracy.c <- ifelse(longDatarecog$accuracy == 1, 0.5, -0.5)

# now the full model
befAftAccMod<-glmer(recogAcc~befAft.c*accuracy.c+(1+befAft.c*accuracy.c|SubNum) , family=binomial(), data= longDatarecog[longDatarecog$listN!=2 & longDatarecog$confidence>2,],
                    control=glmerControl(optimizer="bobyqa",optCtrl=list(maxfun=100000)))

summary(befAftAccMod)

Anova(befAftAccMod)

 befAftAccModnoInt<-glmer(recogAcc~befAft+accuracy+(1+befAft+accuracy|SubNum) , family=binomial(), data= longDatarecog[longDatarecog$listN!=2 & longDatarecog$confidence>2,],
                    control=glmerControl(optimizer="bobyqa",optCtrl=list(maxfun=100000)))

summary(befAftAccModnoInt)
 
Anova(befAftAccModnoInt) 

# with ranodm intercepts for image (takes a long time)
befAftAccModIm<-glmer(recogAcc~befAft*accuracy*listN+(1+befAft*accuracy*listN|SubNum) , family=binomial(), data= longDatarecog[longDatarecog$listN!=2 & longDatarecog$confidence>2,],
                    control=glmerControl(optimizer="bobyqa",optCtrl=list(maxfun=100000)))


summary(befAftAccModIm)

Anova(befAftAccModIm)

longDatarecog$con_weighted_acc<-as.numeric(longDatarecog$con_weighted_acc)
befAftConfAccModInt<-lmer(con_weighted_acc~befAft*accuracy+(1+befAft*accuracy|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,])
summary(befAftConfAccModInt)

Anova(befAftConfAccModInt)

# no int
befAftConfAccMod<-lmer(con_weighted_acc~befAft+accuracy+(1+befAft+accuracy|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,], 
                       control=lmerControl(optimizer="bobyqa",
                                         optCtrl=list(maxfun=100000)))
summary(befAftConfAccMod)

#------------------------------------------------------------------------------#
# plot predictec
#------------------------------------------------------------------------------#
# center
longDatarecog$befAft.c <- ifelse(longDatarecog$befAft== "beforeCP", -0.5, 0.5)

longDatarecog$accuracy.c <- ifelse(longDatarecog$accuracy== "Incorrect", -0.5, 0.5)

# fit the model
befAftConfAccMod.c<-lmer(con_weighted_acc~befAft+accuracy.c+
                           (1+befAft+accuracy.c|SubNum),
                   data= longDatarecog[longDatarecog$listN!=2 ,], 
                       control=lmerControl(optimizer="bobyqa",
                                           optCtrl=list(maxfun=100000)))

summary(befAftConfAccMod.c)

# get predicted values for each 
# intercept
AfterCPAftfix<-fixef(befAftConfAccMod.c)[1]
AfterCPAft_se<-summary(befAftConfAccMod.c)$coefficients[4]
AfterCPAftran<-fixef(befAftConfAccMod.c)[1]+ranef(befAftConfAccMod.c)$SubNum$`(Intercept)`
AfterCPbeffix<-fixef(befAftConfAccMod.c)[1]+fixef(befAftConfAccMod.c)[2]
AfterCPAft_se<-summary(befAftConfAccMod.c)$coefficients[5]
AfterCPbefran<-AfterCPbeffix+ranef(befAftConfAccMod.c)$SubNum$befAftbeforeCP
#intercept_se<-summary(model_red)$coefficients[5]

# create a dataset
predicted<-as.data.frame(cbind("Sub" = rep(1:40, times=2), 
                               "befAft" = rep(c("afterCP", "beforeCP"), each = 40),
                               "conf_weigh_acc" = as.numeric(c(AfterCPAftran,AfterCPbefran ))
                               ))

predicted$conf_weigh_acc<-as.numeric(as.character(predicted$conf_weigh_acc))

# get within participant SE
library(Rmisc)
dat_summary <- summarySEwithin(predicted,
                               measurevar = "conf_weigh_acc",
                               withinvars = c("befAft"), 
                               idvar = "Sub")


# plot 
ggplot(dat_summary, aes(befAft, conf_weigh_acc))+
  geom_bar(aes(befAft, conf_weigh_acc, fill = befAft), position="dodge",stat="summary", fun.y="mean", SE=T)+

  #stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  geom_errorbar(aes(y = conf_weigh_acc, ymin = conf_weigh_acc - ci, ymax = conf_weigh_acc + ci),
                color = "black", width = 0.10, size =1, data=dat_summary)+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_classic()+
  geom_jitter(data = predicted, aes(befAft, conf_weigh_acc,group = Sub), size=1,
              width = 0.1, alpha=0.2, stat="summary")+
     theme(
    plot.title = element_text(size = 30),
    axis.title.x = element_text(size = 27),
    axis.title.y = element_text(size = 27),
    axis.text=element_text(size=27)
     )+
  xlab("Change Point")+
    scale_x_discrete(labels = c('After','Before'))+
      theme(axis.title=element_text(size=14,face="bold"))+
  #facet_grid(.~rec_session)+
  #theme(strip.text.x = element_text(size = 13))+ 
  ylab("Conf-weighted accuracy")+
  coord_cartesian(ylim=c(5,7.5))+  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  #theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")





Anova(befAftConfAccMod)
befAftConfAccMod<-lmer(con_weighted_acc~befAft+(1+befAft|SubNum)+(1|image),
                   data= longDatarecog[longDatarecog$listN!=2 & longDatarecog$SubNum<21,])
summary(befAftConfAccMod)

# now only high confidence
befAftAccModConf<-glmer(recogAcc~befAft+(1+befAft|SubNum), family=binomial(), data= longDatarecog[longDatarecog$confidence>=3&longDatarecog$listN!=2,],
                    control=glmerControl(optimizer="bobyqa",optCtrl=list(maxfun=100000)))
summary(befAftAccModConf)

# with accuracy
befAftAccModConf<-glmer(recogAcc~befAft*accuracy+(1+befAft*accuracy|SubNum), family=binomial(), data= longDatarecog[longDatarecog$confidence>=3&longDatarecog$listN!=2,],
                    control=glmerControl(optimizer="bobyqa",optCtrl=list(maxfun=100000)))
summary(befAftAccModConf)

Anova(befAftAccModConf)

befAftAccModConf<-glmer(recogAcc~befAft+(1+befAft|SubNum)+(1|image), family=binomial(), data= longDatarecog[longDatarecog$confidence>=3&longDatarecog$listN!=2,],
                    control=glmerControl(optimizer="bobyqa",optCtrl=list(maxfun=100000)))
summary(befAftAccModConf)

# what if we exclude lower performer on the encoding?
partExcl<-c(28, 32, 22)


# exclude the 1 and the 9 
# Participants that we want to exclude

# code to exclude participants
longDatarecogExcl<-longDatarecog[!longDatarecog$SubNum %in% partExcl, ]


befAftAccModExcl<-glmer(recogAcc~befAft+(1+befAft|SubNum), family=binomial(), data= longDatarecogExcl[longDatarecogExcl$listN!=2,])
summary(befAftAccModExcl)

# with accuracy
befAftAccModExclAcc<-glmer(recogAcc~befAft+accuracy+(1+befAft+accuracy|SubNum), family=binomial(), data= longDatarecogExcl[longDatarecogExcl$confidence>=3&longDatarecogExcl$listN!=2,])

summary(befAftAccModExclAcc)

# now conf weight
source ("helper_functions/getConfWeight.R")
longDatarecogExcl<-getConfWeight(longDatarecogExcl)

befAftAccModExclAcc<-lmer(confWrec~befAft+(1+befAft|SubNum),
                          data= longDatarecogExcl[longDatarecogExcl$listN!=2 &longDatarecogExcl$confidence>=3,])

summary(befAftAccModExclAcc)
# reaction times
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, RTrecog))+ 
  geom_bar(aes(befAft, RTrecog, fill = befAft),
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  #facet_grid(.~listN)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % RT")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20, angle = 45))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")

# as a function of accuracy
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, RTrecog))+ 
  geom_bar(aes(befAft, RTrecog, fill = befAft),
           position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
  theme_bw()+
  facet_grid(.~recogAcc)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % RT")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20, angle = 45))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")


#by accuracy
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(befAft, recogAcc))+ geom_bar(aes(befAft, recogAcc, fill = befAft),
                                                                                position="dodge",stat="summary", fun.y="mean", SE=T)+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  #geom_jitter( size=1,width=0.1)+
  theme(axis.text.x = element_blank())+ # we are showing the different levels through the colors, so we can avoid naming the bars
  theme(axis.ticks.x = element_blank())+
    geom_line(data = groupData, aes(befAft, recogAcc,group = SubNum), size=1, alpha=0.2, stat="summary")+

  theme_bw()+
    coord_cartesian(ylim=c(0.5,1))+  # xlab("feedback")+

  #facet_grid(.~rec_session)+
  theme(strip.text.x = element_text(size = 13))+ 
  ylab(" % Hit")+
  # xlab("feedback")+
  scale_fill_manual("legend", values = c("afterCP" = "darkgoldenrod1", "beforeCP" = "darkred"))+
  theme(axis.text.x = element_text( size = 20))+
  theme(axis.title=element_text(size=14,face="bold"))+
  theme(legend.position = "none")+
  facet_wrap(.~accuracy)

# now by time from change point
ggplot(longDatarecog[longDatarecog$listN!=2,], aes(NfromChange, recogAcc))+
  geom_line(stat="smooth",method = "glm",  formula=y~x,method.args=list(family="binomial"), alpha=0.5)+
  aes(colour = factor(SubNum))+
  geom_smooth(method="glm",formula=y~x,method.args=list(family="binomial"), colour="black", se=T)+
  theme(strip.text.x = element_text(size = 13))+
  facet_wrap(.~accuracy)+
  theme_bw()+
  theme_bw()

befAftNfromchange<-glmer(recogAcc~NfromChange*accuracy+(1+NfromChange*accuracy|SubNum), family=binomial(), data= longDatarecog[longDatarecog$listN!=2 & longDatarecog$confidence>=3,])
summary(befAftNfromchange)



```

