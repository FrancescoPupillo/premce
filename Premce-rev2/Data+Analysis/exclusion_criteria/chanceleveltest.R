# simulate data
# create several values of probability

# number of trials
ntrials<-20

prob <- seq(0.3, 1, length.out = 100)
psign<-vector()
for (sim in 1:100){
for (j in 1:length(prob)){
  
  p<- prob[j]
  
  t<-vector()
for (i in 1: 100) {
a<-sample(c(1,0,0,0), size=ntrials, prob=c(p, (1-p)/3,(1-p)/3,(1-p)/3) , replace=T)

# chance level
aAlt<-sample(c(1,0,0,0), size=ntrials, prob=c(0.25, 0.25,0.25,0.25 ), replace=T)

t[i]<- t.test(a,aAlt )$p.value

}
psign[j]<-mean(t, na.rm=T)

}
  
}

prob[psign<0.05][1]

plot(prob, psign)
abline(h = 0.05)

p<-0.60
a<-sample(c(1,0,0,0), size=20, prob=c(p, (1-p)/3,(1-p)/3,(1-p)/3) , replace=T)

aAlt<-sample(c(1,0,0,0), size=20, prob=c(0.25, 0.25,0.25,0.25 ), replace=T)

t.test(a,aAlt )

sum(a)
    
24/40
